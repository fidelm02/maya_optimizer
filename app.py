"""
Master script.

This script.
"""
import sys
from PySide import QtGui, QtCore
from Optimizer.UI.BaseInterface import Ui_MainWindow
from Optimizer.UI.BaseStatus import Ui_form_status
from Optimizer.UI.InterfaceFunctions import InterfaceActions
from Optimizer.UI.BaseDetails import Ui_win_details


class MyApplication(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MyApplication, self).__init__(parent)
        self.setupUi(self)


class StatusApplication(QtGui.QMainWindow, Ui_form_status):
    def __init__(self, parent=None):
        super(StatusApplication, self).__init__(parent)
        self.setupUi(self)


class DetailWindow(QtGui.QMainWindow, Ui_win_details):
    def __init__(self, parent=None):
        super(DetailWindow, self).__init__(parent)
        self.setupUi(self)


if __name__ == "__main__":
    """
    Title.

    Description
    """
    if QtCore.QCoreApplication.instance() is not None:
        app = QtCore.QCoreApplication.instance()
    else:
        app = QtGui.QApplication(sys.argv)
    app.aboutToQuit.connect(app.deleteLater)

    window = MyApplication()
    status_window = StatusApplication()
    details_window = DetailWindow()

    window.setWindowFlags(
        window.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)

    details_window.setWindowFlags(
        details_window.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)

    interfaceMacho = InterfaceActions(
        window_interface=window, status_window=status_window,
        detail_window=details_window)
    window.show()
    try:
        sys.exit(app.exec_())
    except:
        ""
