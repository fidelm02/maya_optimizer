"""
Shading.

Description
"""
from .General import General


class ShadingControls(General):
    """
    Title.

    Description
    """

    def __init__(self):
        """
        Title.

        Description
        """
        # super(ShadingControls, self).__init__()
        General.__init__(self)

    def repeated_shaders(self, fix):
        """
        Title.

        Description.
        """
        if fix:
            self.posible_fix.append("repeated shaders fix")
        else:
            self.posible_fix.append("repeated shaders check")

    def external_shaders(self, fix):
        """
        Title.

        Description.
        """
        if fix:
            self.posible_fix.append("external shaders fix")
        else:
            self.posible_fix.append("external shaders check")

    def local_textures(self, fix):
        """
        Title.

        Description.
        """
        if fix:
            self.posible_fix.append("local textures fix")
        else:
            self.posible_fix.append("local check")
