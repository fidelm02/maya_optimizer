"""
Modeling.

This is the master class that contains all methods
for modeling optimization.
"""

from .General import General
import maya.cmds as cmds


class ModelingControls(General):
    """
    Title.

    Description
    """

    def __init__(self):
        """
        Title.

        Description
        """
        General.__init__(self)

    def transformations(self, fix):
        """
        First line.

        Description.
        """
        all_geos = cmds.ls(geometry=1)
        attrs = ['X', 'Y', 'Z']
        problem = False
        problems_active = False

        for geo in all_geos:
            current_problem = False
            geo = cmds.listRelatives(geo, p=1)[0]
            for attr in attrs:
                if cmds.getAttr(geo + '.translate' + attr) is not 0:
                    problem = True
                    current_problem = True
                if cmds.getAttr(geo + '.rotate' + attr) is not 0:
                    problem = True
                    current_problem = True
                if cmds.getAttr(geo + '.scale' + attr) is not 1:
                    problem = True
                    current_problem = True

            if current_problem:
                if problems_active is False:
                    self.details.append("Transformations problems")
                    problems_active = True
                if fix:
                    cmds.lockNode(geo, lock=0)
                    cmds.makeIdentity(geo, apply=1, t=1, r=1, s=1, n=0)
                    self.details.append("\tModified: " + str(geo))
                else:
                    self.details.append("\t" + str(geo))
                    print str(geo)

        if problem:
            self.posible_fix.append("Transformation problems")
        else:
            self.posible_fix.append("All transformations: Clear")

    def history(self, fix):
        """
        First line.

        Description.
        """
        print("history")
        if fix:
            self.posible_fix.append("non_referenced fix")
        else:
            self.posible_fix.append("non_referenced check")

    def materials(self, fix):
        """
        Check materials.

        In the exist only should exist initial materials.
        """

        first_message = True
        not_delete = [
            'defaultShaderList1', 'lambert1', 'particleCloud1',
            'initialParticleSE', 'initialShadingGroup',
            'initialMaterialInfo', 'materialInfo1']

        all_mat_tex = cmds.ls(tex=1, mat=1)
        elements = []
        for current in all_mat_tex:
            allowed = False
            for allowed_type in not_delete:
                if str(current).count(allowed_type):
                    allowed = True
            if not allowed:
                elements.append(current)

        all_mat_tex_info = cmds.ls(type='meterialInfo')
        if len(all_mat_tex_info) > 1:
            for current in all_mat_tex_info:
                elements.append(current)

        if len(elements):
            for shad_or_tex in elements:
                if cmds.objExists(shad_or_tex):
                    connections = cmds.listConnections(shad_or_tex)
                    for currentNode in connections:
                        cond1 = currentNode not in not_delete
                        cond2 = cmds.objExists(current)
                        if cond1 and cond2:
                            cmds.hyperShae(o=currentNode)
                            cmds.sets(e=1, fe='initialShadingGroup')
                            if currentNode in elements:
                                elements.remove(currentNode)
                            if first_message:
                                self.details("  Materials")
                                self.posible_fix.append("Materials and textures")
                                first_message = False
                            if fix:
                                self.posible_fix.append("\t Removed: " + str(currentNode))
                                self.details.append("\t Removed: " + str(currentNode)
                                cmds.lockNode(currentNode, l=0)
                                cmds.delete(currentNode)
                            else:
                                self.posible_fix.append("\t Not allowed: " + str(currentNode))
                                self.details.append("\t  Not allowed: " + str(currentNode)
                        if shad_or_tex not in not_delete:
                            if cmds.objExists(shad_or_tex):
                                if first_message:
                                    self.details("  Materials")
                                    self.posible_fix.append("Materials and textures")
                                    first_message = False
                                if fix:
                                    self.posible_fix.append("\t Removed: " + str(shad_or_tex))
                                    self.details.append("\t Removed: " + str(shad_or_tex)
                                    cmds.lockNode(shad_or_tex, l=0)
                                    cmds.delete(shad_or_tex)
                                else:
                                    self.posible_fix.append("\t Not allowed: " + str(shad_or_tex))
                                    self.details.append("\t Not allowed: " + str(shad_or_tex)

    def uv_sets(self, fix):
        """
        First line.

        Description.
        """
        if fix:
            self.posible_fix.append("uv sets fix")
        else:
            self.posible_fix.append("uv sets check")

    def center(self, fix):
        """
        Geometry in the center.

        Detect if are only one geometry in the scene.
        """
        geos = cmds.ls(geometry=1)
        root_nodes = []
        main_root = []
        feedback_header = False
        for geo in geos:
            root_nodes.append(self.get_root_node(element=geo))

        for i in range(0, len(root_nodes) - 1):
            current = root_nodes[i]
            repeated = False
            if i is not len(root_nodes) - 1:
                for j in range(i + 1, len(root_nodes) - 1):
                    evaluated = root_nodes[j]
                    if current == evaluated:
                        repeated = True
            if repeated is False:
                main_root.append(current)

        for root_node in main_root:
            corners = cmds.exactWorldBoundingBox(root_node)

            button_point = [
                (corners[0] + corners[3]) / 2, corners[1],
                (corners[2] + corners[5]) / 2]

            wrong = False
            for position in button_point:
                if position != 0:
                    wrong = True

            if wrong:
                if feedback_header is False:
                    feedback_header = True
                    self.posible_fix.append(" Center elements")
                if fix:
                    print root_node
                    print button_point
                    cmds.xform(root_node, piv=button_point, ws=1)
                    cmds.move(0, 0, 0, root_node, rpr=1)
                    cmds.makeIdentity(root_node, apply=1, t=1, r=1, s=1, n=0)

                    str_fix = (str(root_node) + " was moved")
                    self.posible_fix.append("\t" + str_fix)
                else:
                    str_chk = (str(root_node) + " needs to be fixed")
                    self.posible_fix.append("\t" + str_chk)

    def get_root_node(self, element):
        """
        Recursivity method.

        This method will serch the father node
        until the current node doesn't have a prodecesor node.
        """
        temp_element = cmds.listRelatives(element, p=1)
        if temp_element is None:
            return element[0]
        else:
            return self.get_root_node(element=temp_element)
