"""
General.

Description
"""

import maya.cmds as cmds


class General():

    """
    Title.

    Description
    """

    def __init__(self):
        """
        Title.

        Description
        """
        self.posible_fix = []
        self.reference_to_fix = []
        self.details = []

    def unknown_nodes(self, fix):
        """
        Unkwnon and disconnected nodes.

        This methos will delete all unnecesary
        nodes from the scene.
        """
        useless_nodes = []
        element_types = [
            'groupId', 'hyperGraphInfo', 'hyperLayout',
            'hyperView', 'reference', 'mesh']

        # Put the elements to delete into the useless_node list
        for current_type in element_types:
            elements = cmds.ls(type=current_type)
            for element in elements:
                if current_type is 'mesh' or 'groupId':
                    if cmds.listConnections(element) is None:
                        useless_nodes.append(element)
                if current_type is 'hyperGraphInfo':
                    if element is not 'hyperGraphInfo':
                        useless_nodes.append(element)
                if current_type is 'hyperLayout':
                    if element is not 'hyperGraphLayout':
                        useless_nodes.append(element)
                if current_type is 'hyperView':
                    if element is not 'hyperView':
                        useless_nodes.append(element)
                if current_type is 'reference':
                    if str(element).count("UNKNOWN_REF_NODE"):
                        useless_nodes.append(element)

        cant = len(useless_nodes)
        ref_problems = []
        if cant > 0:
            self.posible_fix.append("   Unknown nodes")
            text = "\tThe scene contains "
            text = text + str(cant) + "useless elements"
            self.posible_fix.append(text)
            reference_feedback = False
            for element in useless_nodes:
                if fix:
                    cmds.lockNode(element, l=0)
                    try:
                        cmds.delete(element)
                        self.details.append(
                            "\t Unknown: " + str(element) +
                            "deleted.")
                    except:
                        self.details.append(
                            "\t Unknown referenced: " + str(element))
                        if cmds.referenceQuery(element, isNodeReferenced=True):
                            namespace = cmds.referenceQuery(
                                element, referenceNode=True)
                            ref_problems.append(namespace)
                else:
                    if cmds.referenceQuery(element, isNodeReferenced=True):
                        if reference_feedback is False:
                            reference_feedback = True
                            self.reference_to_fix.append("\t  Unknown referenced nodes in the scene")
                        self.details.append( "\t Unknown referenced: " + str(element)
                            namespace = cmds.referenceQuery(
                                element, referenceNode=True)
                            ref_problems.append(namespace)

    def non_referenced(self, fix):
        """
        Title.

        Description.
        """
        problems = False
        all_geos = cmds.ls(shapes=1)
        all_lights = cmds.ls(lights=1)
        all_lists = [all_geos, all_lights]

        for current_list in all_lists:
            for current_object in current_list:
                if cmds.referenceQuery(current_object, inr=1) == 0:
                    if problems is not True:
                        self.details.append("Non Referenced objects")
                        problems = True

                    print str(current_object) + " is not a reference"
                    if fix:
                        # Add details of the current object
                        self.details.append(
                            "\tDeleted: " + str(current_object))

                        # Unlock the current object (if is needed)
                        cmds.lockNode(current_object, lock=0)

                        # Delete current object
                        cmds.delete(current_object)

                    else:
                        self.details.append(
                            "\t" + str(current_object))

        if problems:
            if fix:
                self.posible_fix.append(
                    "All non referenced objects were deleted.")
            else:

                self.posible_fix.append("Non referenced objects in the scene.")
        else:
            self.posible_fix.append("Non referenced: Clean")

    def return_feedback(self):
        """
        Return feedback.

        This method only return in an array the
        two strings that have the respective feedback.
        """
        posible = self.posible_fix
        reference = self.reference_to_fix
        details = self.details
        feedback_strings = [posible, reference, details]
        self.posible_fix = []
        self.reference_to_fix = []
        self.details = []
        return feedback_strings
